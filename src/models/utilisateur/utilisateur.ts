export class Utilisateur {
    id:number;
    nom:String;
    prenom:String;
    email:String;
    adresse:String;
    telephone:String;
    role:String;
}