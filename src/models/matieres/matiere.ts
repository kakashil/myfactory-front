export class Matiere {
    id: number;
    titre: string;
    duree: number;
    objectifs: string;
    preRequis: number;
    contenu: string;
}