import { Matiere } from '../matieres/matiere';
import { Utilisateur } from '../utilisateur/utilisateur';

export class Competence{
    id:number;
    matiere:Matiere;
    niveau:string;
    formateur:Utilisateur;
}