import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {CompteserviceService} from '../service/compteservice.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  constructor(private  compteService:  CompteserviceService , private router: Router) { }

  email : string = null;
  password : string = null;

  userEnCoursChild;

  ngOnInit() {
  }

  connexion(){
    this.compteService.connectUser(this.email , this.password).subscribe((data:  Object) => {
      this.connexionResponse(data);
    });
  }

  connexionResponse(data : Object ){
    if(data != null){
      alert("Connexion OK !");
      this.compteService.envoiUserEnCours(data);
      this.router.navigate(['']);
    }else{
      alert("Email / Mot de passe invalide !")
    }
  }
  
}
