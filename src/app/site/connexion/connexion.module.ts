import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'; 
import  {ConnexionComponent} from './connexion.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ConnexionComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([       { path: 'connexion', component: ConnexionComponent }     ]) 
  ]
})
export class ConnexionModule { }
