import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormateurComponent } from './formateur.component';

import { RouterModule } from '@angular/router'; 

@NgModule({
  declarations: [FormateurComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([       { path: 'formateur', component: FormateurComponent }     ]) 
  ]
})
export class FormateurModule { }
