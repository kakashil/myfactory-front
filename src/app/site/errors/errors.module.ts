import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ErrorsComponent } from './errors.component'; 

@NgModule({
  declarations: [ErrorsComponent],
  imports: [
    CommonModule,     
    RouterModule.forChild([       
      { path: '404', component: ErrorsComponent },
      { path: '**', redirectTo: '/404'}     
    ]) 
  ]
})
export class ErrorsModule { }
