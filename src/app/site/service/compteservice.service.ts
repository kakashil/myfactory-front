import { Injectable } from '@angular/core';
import { HttpClient , HttpParams ,HttpHeaders} from  '@angular/common/http';
import { Subject }    from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CompteserviceService {

  API_URL  =  'http://localhost:8082/myFactory';

  constructor(private  httpClient:  HttpClient) {}

  userEnCours = null;

  connectUser(email : string , password : string){

    const body = new HttpParams()
    .set('email', email)
    .set('password', password);

    return this.httpClient.post(`${this.API_URL}/connexion/connect`,body.toString(),
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }
    );
  }

  
  // Observable sources
  private userEnCoursSource = new Subject<Object>();

  // Observable streams
  userChange$ = this.userEnCoursSource.asObservable();

  // Service message commands
  envoiUserEnCours(userEnCours: object) {
    this.userEnCoursSource.next(userEnCours);
  }
  
}