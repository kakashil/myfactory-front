import { TestBed } from '@angular/core/testing';

import { CompteserviceService } from './compteservice.service';

describe('CompteserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompteserviceService = TestBed.get(CompteserviceService);
    expect(service).toBeTruthy();
  });
});
