import { Component, OnInit } from '@angular/core';

import { ApiService } from  '../api.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import{Matiere} from '../../../models/matieres/matiere'

@Component({
  selector: 'app-matieres',
  templateUrl: './matieres.component.html',
  styleUrls: ['./matieres.component.css']
})
export class MatieresComponent implements OnInit {

  //Liste des matieres
  private  matieres:  Array<Matiere> = [];

  //Objet matiere pour update
  private matiereUpdate : Matiere = new Matiere();

  //Objet matiere pour création
  private matiereCreation : Matiere = new Matiere();

  constructor(private  apiService:  ApiService , private modalService: NgbModal) { }

  hiddenFormCreate : boolean = true;
  hiddenFormUpdate : boolean = true;

  ngOnInit() {
    this.getMateriels();
  } 

  public  getMateriels(){
    this.apiService.getMatieresList().subscribe((data:  Array<Matiere>) => {
        this.matieres  =  data;
        console.log(data);
    });
  }

  /* Update */
  assignMatiereUpdate(matiere : Matiere ){
    this.matiereUpdate = Object.assign({}, matiere);
    this.hiddenFormUpdate = false;
    this.hiddenFormCreate = true;
  }
  
  updateMatiere(){
    this.apiService.updateMatiere(this.matiereUpdate).subscribe(
      res => this.traitementResultUpdate(res), 
      err => this.traitementResultUpdate(false)
    );
  }

  traitementResultUpdate(result){
    if(result){
      alert("Mise a jour Ok");
      this.matiereUpdate = new Matiere();
      this.hiddenFormUpdate = true;
    }else{
      alert("Erreur lors de la mise a jour");
    }
  }
  
  annuleUpdate(){
    this.hiddenFormUpdate = true;
  }

  /*Suppression */
  supprimeMatiere(matiere : Matiere){
    console.log(matiere.id);
    this.apiService.deleteMatiere(matiere.id).subscribe(
      res => this.traitementResultDelete(res), 
      err => this.traitementResultDelete(false)
    )
  }

  traitementResultDelete(result){
    if(result){
      alert("Suppression Ok");
      this.matieres = [];
      this.getMateriels();
    }else{
      alert("Erreur lors de la suppression");
    }
  }

  /*Creation */

  showMatiereForm(){
    this.hiddenFormCreate = false;
    this.hiddenFormUpdate = true;
  }

  createMatiere(){
    this.apiService.createMatiere(this.matiereCreation).subscribe(
      res => this.traitementResultCreate(res), 
      err => this.traitementResultCreate(false)
    );
  }

  traitementResultCreate(result){
    if(result){
      alert("Creation Ok");
      this.matieres = [];
      this.getMateriels();
      this.hiddenFormCreate = true;
    }else{
      alert("Erreur lors de la creation");
    }
  }

  annuleCreation(){
    this.matiereCreation = new Matiere();
    this.hiddenFormCreate = true;
  }

}


  