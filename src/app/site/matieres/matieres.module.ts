import { NgModule , Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatieresComponent } from './matieres.component';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router'; 


@NgModule({
  declarations: [MatieresComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,

    RouterModule.forChild([       { path: 'matiere', component: MatieresComponent }     ])
  ]
})
export class MatieresModule { }
