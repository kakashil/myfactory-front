import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompteComponent } from './compte.component';
import { RouterModule } from '@angular/router'; 
import { NotifierModule } from 'angular-notifier';

@NgModule({
  declarations: [CompteComponent],
  imports: [
    CommonModule,
    NotifierModule,
    RouterModule.forChild([       { path: 'compte', component: CompteComponent }     ]) 
  ]
})
export class CompteModule { }
