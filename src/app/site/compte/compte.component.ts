import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})

  
export class CompteComponent implements OnInit {
  
  constructor() {}
  ngOnInit() {
}
public EnabledUpdate() {
  var container, inputs, index;

  // Get the container element
  container = document.getElementById('formCompte');

  // Find its child `input` elements
  inputs = container.getElementsByTagName('input');
  for (index = 0; index < inputs.length; ++index) {
    container[index].removeAttribute("disabled");
  }
}


public DisabledUpdate() {
  var container, inputs, index;

  // Get the container element
  container = document.getElementById('formCompte');

  // Find its child `input` elements
  inputs = container.getElementsByTagName('input');
  for (index = 0; index < inputs.length; ++index) {
    container[index].setAttribute("disabled", false);
  }
}
}
