import { Component, OnInit } from '@angular/core';
import { Competence } from 'src/models/competences/competence';
import { ApiService } from '../api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompetenceCrea } from 'src/models/competences/competence.creation';
import { CompUpdate } from 'src/models/competences/competence.update';

@Component({
  selector: 'app-competence',
  templateUrl: './competence.component.html',
  styleUrls: ['./competence.component.css']
})
export class CompetenceComponent implements OnInit {

  private competences : Array<Competence> = [];

  private competenceUpdate : Competence = new Competence();
  private compUpdate : CompUpdate = new CompUpdate();
  private competenceCreation : CompetenceCrea = new CompetenceCrea();
  constructor(private apiService : ApiService, private modelService : NgbModal) { }

  hiddenFormCreate : boolean = true;
  hiddenFormUpdate : boolean = true;

  ngOnInit() {
    this.getCompetence();
  }

  public getCompetence(){
    this.apiService.getCompetenceList().subscribe((data : Array<Competence>)=>{
        this.competences = data;
        console.log(data);
    })
  }

  /*Update*/
  public assignCompetenceUpdate(competence : Competence){
      this.competenceUpdate = Object.assign({}, competence);
      this.hiddenFormUpdate = false;
      this.hiddenFormCreate = true;
  }

  public updateCompetence(){
    this.compUpdate.id = this.competenceUpdate.id;
    this.compUpdate.idUtil = this.competenceUpdate.formateur.id;
    this.compUpdate.idMat = this.competenceUpdate.matiere.id;
    this.compUpdate.niveau = this.competenceUpdate.niveau;
    this.apiService.updateCompetence(this.compUpdate).subscribe(
      res => this.traitementResultUpdate(res),
      err => this.traitementResultUpdate(false)
    );
  }

  public traitementResultUpdate(result){
    if(result)
    {
      alert("Mise à jour OK");
      this.competenceUpdate = new Competence();
      this.hiddenFormUpdate = true;
    }
    else {
      alert("Echec de la mise à jour");
    }
  }

  annuleUpdate(){
    this.hiddenFormUpdate = true;
  }

  /*Suppression */
  supprimeCompetence(competence : Competence)
  {
    console.log(competence.id);
    this.apiService.deleteCompetence(competence.id).subscribe(
      res => this.traitementResultDelete(res),
      err => this.traitementResultDelete(false)
    );
  }

  traitementResultDelete(result){
    if(result)
    {
      alert("Suppression OK");
      this.competences = [];
      this.getCompetence();
    } else {
      alert("Echec de la suppression");
    }
  }

  /*Creation */
  showCompetenceForm(){
    this.hiddenFormCreate = false;
    this.hiddenFormUpdate = true;
  }

  createCompetence(){
    this.apiService.createCompetence(this.competenceCreation).subscribe(
      res => this.traitementResultCreate(res),
      err => this.traitementResultCreate(false)
    );
  }

  traitementResultCreate(result){
      if(result)
      {
        alert("Création OK");
        this.competences = [];
        this.getCompetence();
        this.hiddenFormCreate = true;
      } else
      {
        alert("Echec lors de la création");
      }
  }

  annuleCreation()
  {
    this.competenceCreation = new CompetenceCrea();
    this.hiddenFormCreate = true;
  }
}
