import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompetenceComponent } from './competence.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CompetenceComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,

    
    RouterModule.forChild([       { path: 'competence', component: CompetenceComponent }     ])
  ]
})
export class CompetenceModule { }
