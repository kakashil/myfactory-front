import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
import { Matiere } from 'src/models/matieres/matiere';
import { Utilisateur } from 'src/models/utilisateur/utilisateur';
import { Competence } from 'src/models/competences/competence';
import { CompetenceCrea } from 'src/models/competences/competence.creation';
import { CompUpdate } from 'src/models/competences/competence.update';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API_URL  =  'http://localhost:8082/myFactory';

  constructor(private  httpClient:  HttpClient) {}

  getMatieresList(){
    return  this.httpClient.get(`${this.API_URL}/matieres/list`);
  }

  updateMatiere(matiere : Matiere){
    return this.httpClient.put<Matiere>(`${this.API_URL}/matieres/update`,matiere);
  } 
  
  deleteMatiere(id : number){
    return this.httpClient.delete(`${this.API_URL}/matieres/delete/`+id);
  }

  createMatiere(matiere : Matiere){
    return this.httpClient.post<Matiere>(`${this.API_URL}/matieres/creer`,matiere);
  }

  //UTILISATEUR
  getUtilisateurList() {
    return this.httpClient.get(`${this.API_URL}/users/list`)
  }

  updateUtilisateur(utilisateur : Utilisateur) {
    return this.httpClient.put<Utilisateur>(`${this.API_URL}/users/updateUtil`,utilisateur); 
  }

  deleteUtilisateur(id : number) {
    return this.httpClient.delete(`${this.API_URL}/users/delete/`+id);
  }
  
  createUtilisateur(utilisateur : Utilisateur){
    return this.httpClient.post<Utilisateur>(`${this.API_URL}/users/creer`, utilisateur);
  }

  //COMPETENCE

  getCompetenceList(){
    return this.httpClient.get(`${this.API_URL}/competence/list`);
  }

  updateCompetence(competence : CompUpdate){
    return this.httpClient.put<Competence>(`${this.API_URL}/competence/update`,competence);
  }

  deleteCompetence(id : number){
    return this.httpClient.delete(`${this.API_URL}/competence/delete/` + id);
  }

  createCompetence(competence : CompetenceCrea){
    return this.httpClient.post<Competence>(`${this.API_URL}/competence/creer`, competence);
  } 

}
