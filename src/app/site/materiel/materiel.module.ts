import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterielComponent } from './materiel.component';

import { RouterModule } from '@angular/router'; 

@NgModule({
  declarations: [MaterielComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([       { path: 'materiel', component: MaterielComponent }     ]) 
  ]
})
export class MaterielModule { }
