import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../api.service';
import { Matiere } from 'src/models/matieres/matiere';

@Component({
  selector: 'app-materiel',
  templateUrl: './materiel.component.html',
  styleUrls: ['./materiel.component.css']
})
export class MaterielComponent implements OnInit {

  private matieres : Array<Matiere> = [];

  constructor(private apiService : ApiService, private modelService : NgbModal) { }

  ngOnInit() {
    this.getMateriel();
  }
  public getMateriel() {
    this.apiService.getMatieresList().subscribe((data : Array<Matiere>) => {
    this.matieres = data;
    })
  }

}
