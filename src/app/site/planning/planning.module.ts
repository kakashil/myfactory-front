import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningComponent } from './planning.component';
import { RouterModule } from '@angular/router'; 

@NgModule({
  declarations: [PlanningComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([       { path: 'planning', component: PlanningComponent }     ])
  ]
})
export class PlanningModule { }
