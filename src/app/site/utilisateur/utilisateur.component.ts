import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service'
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Utilisateur} from '../../../models/utilisateur/utilisateur'

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

  //Liste des utilisateurs
  private utilisateurs: Array<Utilisateur> = [];

    //Objet utilisateur pour update
    private utilisateurUpdate : Utilisateur = new Utilisateur();

    // Objet utilisateur pour create
    private utilisateurCreation : Utilisateur = new Utilisateur();

  constructor(private  apiService:  ApiService , private modalService: NgbModal) { }

  hiddenFormCreate : boolean = true;
  hiddenFormUpdate : boolean = true;

  ngOnInit() {
    console.log("utilisateur init");
    this.getUtilisateurs();
  }

  public getUtilisateurs(){
    this.apiService.getUtilisateurList().subscribe((data: Array<Utilisateur>) => {
    this.utilisateurs = data;
    console.log(data);
  } );
}
assignUtilisateurUpdate(utilisateur : Utilisateur ){
  this.utilisateurUpdate = Object.assign({}, utilisateur);
  this.hiddenFormUpdate = false;
  this.hiddenFormCreate = true;
}

updateUtilisateur() {
  console.log("Update utilisateur");
  this.apiService.updateUtilisateur(this.utilisateurUpdate).subscribe(
    res => this.traitementResultUpdate(res), 
    err => this.traitementResultUpdate(false)
  );
}

traitementResultUpdate(result){
  if(this.apiService.updateUtilisateur(this.utilisateurUpdate)){
    alert("Mise a jour Ok");
    this.utilisateurUpdate = new Utilisateur();
    this.hiddenFormUpdate = true;
  }else{
    alert("Erreur lors de la mise a jour");
  }
}

annuleUpdate(){
  this.hiddenFormUpdate = true;
}

/* suppression utilisateur */

supprimeUtlisateur(utilisateur : Utilisateur) {
  console.log("id util " + utilisateur.id);
  this.apiService.deleteUtilisateur(utilisateur.id).subscribe(
    res => this.traitementResultDelete(res),
    err => this.traitementResultDelete(false)
  )
}

traitementResultDelete(result){
  if(result){
    alert("Suppression Ok");
    this.utilisateurs = [];
    this.getUtilisateurs();
  }else{
    alert("Erreur lors de la suppression");
  }
}

 /*Creation */

 showUtilisateurForm(){
  this.hiddenFormCreate = false;
  this.hiddenFormUpdate = true;
}

createUtilisateur() {
  this.apiService.createUtilisateur(this.utilisateurCreation).subscribe(
    res => this.traitementResultCreate(res), 
    err => this.traitementResultCreate(false)
  );
}

  traitementResultCreate(result){
    if(this.apiService.createUtilisateur(this.utilisateurCreation)){
      alert("Creation Ok");
      this.utilisateurs = [];
      this.getUtilisateurs();
      this.hiddenFormCreate = true;
    }else{
      alert("Erreur lors de la creation");
    }
  }
  annuleCreation(){
    this.utilisateurCreation = new Utilisateur();
    this.hiddenFormCreate = true;
  }

}
