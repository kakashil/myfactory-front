import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';

import {CompteserviceService} from '../../service/compteservice.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavmenuComponent implements OnInit {

  constructor(private  compteService:  CompteserviceService , private router: Router) { 

    compteService.userChange$.subscribe(
      user => {
        this.userEnCoursChild = user;
        this.userChange();
      }
    )
  }

  userEnCoursChild;

  private  hasAccesToMatiere : boolean = true;
  private  hasAccesToFormatieur : boolean = true;
  private  hasAccesToPlanning : boolean = true;
  private  hasAccesToMateriel : boolean = true;
  private  hasAccesToCompetence : boolean = true;
  private  hasAccesToCompte : boolean = true;
  private  hasAccesToUtilisateur : boolean = true;
  private isConnected : boolean = true;

  ngOnInit() {
    //this.userEnCoursChild =  this.compteService.getUserEnCours();

  }
  
  deconnexion(){
    this.compteService.envoiUserEnCours(null);
    this.router.navigate(['']);
  }

  userChange(){
    if(this.userEnCoursChild != null){
      console.log("userEnCours ="+this.userEnCoursChild);
      console.log(this.userEnCoursChild);
        this.chargeAcces(this.userEnCoursChild.role);
     }else{
       console.log("userEnCours ="+this.userEnCoursChild);
       this.accesVisiteur();
     }
  }

  chargeAcces(role){
    if(role == 'ADMIN'){
      this.accesAdmin();
    }else if(role == 'FORMATEUR'){
      this.accesFormateur();
    }else if(role == 'GESTIONNAIRE'){
      this.accesGestionnaire();
    }else if(role == 'TECHNICIEN'){
      this.accesTechnicien();
    }
  }

  accesAdmin(){
    this.hasAccesToMatiere = true;
    this.hasAccesToFormatieur = true;
    this.hasAccesToPlanning = true;
    this.hasAccesToMateriel = true;
    this.hasAccesToCompetence = true;
    this.hasAccesToCompte = true;
    this.hasAccesToUtilisateur = true;
    this.isConnected = true;
  }

  accesFormateur(){
    this.hasAccesToMatiere = false;
    this.hasAccesToFormatieur = false;
    this.hasAccesToPlanning = false;
    this.hasAccesToMateriel = false;
    this.hasAccesToCompetence = true;
    this.hasAccesToCompte = true;
    this.hasAccesToUtilisateur = false;
    this.isConnected = true;
  }

  accesGestionnaire(){
    this.hasAccesToMatiere = true;
    this.hasAccesToFormatieur = true;
    this.hasAccesToPlanning = true;
    this.hasAccesToMateriel = true;
    this.hasAccesToCompetence = false;
    this.hasAccesToCompte = true;
    this.hasAccesToUtilisateur = true;
    this.isConnected = true;
  }

  accesTechnicien(){
    this.hasAccesToMatiere = false;
    this.hasAccesToFormatieur = false;
    this.hasAccesToPlanning = false;
    this.hasAccesToMateriel = true;
    this.hasAccesToCompetence = false;
    this.hasAccesToCompte = true;
    this.hasAccesToUtilisateur = false;
    this.isConnected = true;
  }

  accesVisiteur() {
    this.hasAccesToMatiere = false;
    this.hasAccesToFormatieur = false;
    this.hasAccesToPlanning = false;
    this.hasAccesToMateriel = false;
    this.hasAccesToCompetence = false;
    this.hasAccesToCompte = false;
    this.hasAccesToUtilisateur = false;
    this.isConnected = false;
  }

}
