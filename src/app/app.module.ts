import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';

import {HomeModule} from "./site/home/home.module";
import {MatieresModule} from "./site/matieres/matieres.module";
import {FormateurModule} from "./site/formateur/formateur.module";
import {PlanningModule} from "./site/planning/planning.module";
import {MaterielModule} from "./site/materiel/materiel.module";
import {CompteModule} from "./site/compte/compte.module";
import { UtilisateurModule } from "./site/utilisateur/utilisateur.module";
import { CompetenceModule } from "./site/competence/competence.module";


import {ErrorsModule} from "./site/errors/errors.module";

import { SharedModule } from './site/shared/shared.module';

import { HttpClientModule } from  '@angular/common/http';
import { ConnexionComponent } from './site/connexion/connexion.component';
import { ConnexionModule } from './site/connexion/connexion.module';

@NgModule({
  declarations: [
    AppComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    RouterModule.forRoot([]), 
    
    SharedModule,
    
    HomeModule,
    MatieresModule,
    FormateurModule,
    PlanningModule,
    MaterielModule,
    CompteModule,
    ConnexionModule,
    UtilisateurModule,
    CompetenceModule,


    ErrorsModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
